package modelos;

import estructuras.Arco;
import estructuras.Lista;
import estructuras.Nodo;

/**
 * Clase que representa una estacion del metro de New York
 * @param K tipo del identificador unico de los vertices del grafo 
 */
//TODO La clase debe implementar la interface Nodo
public class Estacion<K extends Comparable<K>> implements Nodo<K>{

	/**
	 * Latitud del estación
	 */
	//TODO declare el atributo latitud
	private double latitud;
	/**
	 * Longitud del estación
	 */
	//TODO Declare el atributo longitud	
	private double longitud;
	/**
	 * Identificador único del paradero (de tipo K)
	 */
	//TODO Declare el identificador del paradero (de tipo K)
	private K identificador;
	
	private Lista<Arco<K,String>> arcos;
	
	/**
	 * Construye un nuevo paradero con un identificador y una ubicacion latitud y la longitud dados
	 * @param id Indentificador unico
	 * @param latitud 
	 * @param longitud 
	 */
	public Estacion(K id, double pLatitud, double pLongitud) {
		identificador=id;
		latitud=pLatitud;
		longitud=pLongitud;
		arcos= new Lista<>();
		//TODO implementar
	}
	
	public K darId(){
		//TODO implementar
		return identificador;
	}
	
	/**
	 * Devuelve la latitud del paradero
	 * @return latitud
	 */
	public double darLatitud() {
		//TODO implementar
		return latitud;
	}
	
	/**
	 * Devuelve la longitud del paradero
	 * @return longitud
	 */
	public double darLongitud() {
		//TODO implementar
		return longitud;
	}
	
	public void agregarArco(Arco<K,String> arc)
	{
		arcos.agregarAlFinal(arc);
	}
	
	public Lista<Arco<K,String>> darListaArcos()
	{
		return arcos;
	}
	
	/**
	 * nombre (latitud, longitud)
	 */
	@Override
	public String toString() {
		return darId() +" ("+darLatitud()+" , "+darLongitud()+")";
	}

	@Override
	public int compareTo(Nodo<K> o) 
	{
		return this.darId().compareTo(o.darId());
	}
	
	
}
