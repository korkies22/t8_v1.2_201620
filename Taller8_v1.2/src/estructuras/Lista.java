package estructuras;

import java.util.Comparator;
import java.util.Iterator;

import javax.swing.text.StyledEditorKit.ItalicAction;

/**
 * Clase que representa la lista encadenada gen�rica a utilizar. <br>
 */
public class Lista<E> implements Iterable<E> {

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Clase que representa a un nodo gen�rico. <br>
	 */
	private class Nodo<E> {

		// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Objeto que se va a guardar
		 */
		private E data;

		/**
		 * Siguiente nodo en la lista encadenada
		 */
		private Nodo<E> siguiente;


		// -----------------------------------------------------------------
		// Constructor
		// -----------------------------------------------------------------

		/**
		 * Crea un nuevo nodo gen�rico. <br>
		 * <b> post: </b> El el contenido del nodo se inicializ� con los datos dados por parametro, su nodo siguiente es nulo.
		 * Crea un nuevo nodo genérico. <br>
		 * <b> post: </b> El el contenido del nodo se inicializó con los datos dados por parametro, su nodo siguiente es nulo.
		 * @param pData Objeto que se va a almacenar.
		 */
		public Nodo(E pData) {
			data = pData;
			siguiente = null;
		}

		// -----------------------------------------------------------------
		// M�todos
		// -----------------------------------------------------------------

		/**
		 * Retorna el objeto almacenado.
		 * @return Objeto almacenado.
		 */
		public E darData() {
			return data;
		}

		/**
		 * Retorna el siguiente de la cadena.
		 * @return Nodo siguiente de la cadena.
		 */
		public Nodo<E> darSiguiente() {
			return siguiente;
		}

		/**
		 * Asigna al nodo siguiente el dado por parametro
		 */
		public void asignarSiguiente(Nodo<E> pSiguiente) {
			siguiente = pSiguiente;
		}
	}

	/**
	 * Clase que representa el iterador para moverse a trav�s de los elementos guardados. <br>
	 */
	private class Iterador implements Iterator<E> {

		// -----------------------------------------------------------------
		// Atributos
		// -----------------------------------------------------------------

		/**
		 * Nodo actual de la iteración.
		 */
		private Nodo<E> actual;

		// -----------------------------------------------------------------
		// Constructor
		// -----------------------------------------------------------------
		/**
		 * Crea un nuevo iterador. <br>
		 * <b> post: </b> El iterador se creó con su primer elemento siendo el principio de la lista
		 * @param pData principio, el primer nodo de la lista a iterar.
		 */
		public Iterador(Nodo<E> principio) {
			actual = principio;
		}

		// -----------------------------------------------------------------
		// Métodos
		// -----------------------------------------------------------------
		/**
		 * Retorna si hay un siguiente elemento para recorrer.
		 * @return true si el nodo siguiente no es nulo, false de lo contrario.
		 */
		public boolean hasNext() {
			boolean rta = true;
			if (actual == null) {
				rta = false;
			}
			return rta;
		}

		/**
		 * Retorna el nodo siguiente de la iteración.
		 * @return Nodo siguiente de la iteración.
		 */
		public E next() {
			E data = actual.darData();
			actual = actual.darSiguiente();
			return data;
		}

		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Nodo inicio de la lista
	 */
	private Nodo<E> primero;

	/**
	 * Nodo final de la lista
	 */
	private Nodo<E> ultimo;

	/**
	 * Tama�o actual de la lista
	 */
	private int tam;

	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	/**
	 * Crea una nueva lista gen�rica. <br>
	 * <b> post: </b> Se cre� una lista encadenada gen�rica vac�a.
	 */
	public Lista() {
		primero = null;
		ultimo = null;
		tam = 0;
	}
	public Lista(E data){
		primero = null;
		ultimo = null;
		tam=0;
		agregarAlPrincipio(data);
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Agrega un objeto al principio de la lista.
	 */
	public void agregarAlPrincipio(E data) {
		Nodo<E> n = new Nodo<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			n.asignarSiguiente(primero);
			primero = n;
		}
		tam++;
	}

	/**
	 * Agrega un objeto al final de la lista.
	 */
	public void agregarAlFinal(E data) {
		Nodo<E> n = new Nodo<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			ultimo.asignarSiguiente(n);
			ultimo = ultimo.darSiguiente();
		}
		tam++;
	}

	/**
	 * Retorna el primer nodo de la lista.
	 * @return primero, primer nodo de la lista
	 */
	public E darPrimero() {
		if(primero!=null){
			return primero.darData();
		}
		return null;
	}

	/**
	 * Retorna el �ltimo nodo de la lista.
	 * @return �ltimo, �ltimo nodo de la lista
	 */
	public E darUltimo() {
		return ultimo.darData();
	}

	/**
	 * Elimina y retorna el primer elemento de la lista.
	 * @return primer elemento de la lista.
	 */
	public E quitarPrimero() {
		E data = primero.darData();
		primero = primero.darSiguiente();
		tam--;
		return data;
	}

	/**
	 * Devuelve el tama�o actual de la lista.
	 * @return tama�o actual.
	 */
	public int size() {
		return tam;
	}

	/**
	 * Devuelve si la lista se encuentra actualmente vac�a.
	 * @return true si la lista se encuentra vac�a, false de lo contrario.
	 * Devuelve si la lista se encuentra actualmente vacía.
	 * @return true si la lista se encuentra vacía, false de lo contrario.
	 */
	public boolean isEmpty() {
		return (tam == 0);
	}

	/**
	 * Devuelve un iterador para la lista.
	 * @return iterator.
	 */
	public Iterator<E> iterator() {
		return new Iterador(primero);
	}

	/**
	 * M�todo que ordena la lista utilizando MergeSort.
	 */
	public void sort(Comparator<E> c) {
		Lista<E> l = sort(c, this);
		ultimo = l.ultimo;
		primero = l.primero;
	}

	/**
	 * M�todo recursivo para hacer el sort de Mergesort.
	 */
	public Lista<E> sort(Comparator<E> c, Lista<E> l) {
		if (l.size() <= 1) {
			return l;
		} else {
			Lista<E> l1 = new Lista<E>();
			Lista<E> l2 = new Lista<E>();
			Iterator<E> i_s = l.iterator();
			Iterator<E> i_f = l.iterator();
			int n = 0;
			while (i_f.hasNext()) {
				i_f.next();
				if (n % 2 == 0) {
					l1.agregarAlFinal(i_s.next());
				}
				n++;
			}
			while (i_s.hasNext()) {
				l2.agregarAlFinal(i_s.next());
			}
			l1 = sort(c, l1);
			l2 = sort(c, l2);
			l = merge(l1, l2, c);
			return l;
		}
	}

	/**
	 * M�todo que hace el merge de MergeSort.
	 */
	private Lista<E> merge(Lista<E> l1, Lista<E> l2, Comparator<E> c) {
		Lista<E> rta = new Lista<E>();
		while (!l1.isEmpty() && !l2.isEmpty()) {
			int n = c.compare(l1.darPrimero(), l2.darPrimero());
			if (n <= 0) {
				rta.agregarAlFinal(l1.quitarPrimero());
			} else {
				rta.agregarAlFinal(l2.quitarPrimero());
			}
		}
		while (!l1.isEmpty()) {
			rta.agregarAlFinal(l1.quitarPrimero());
		}
		while (!l2.isEmpty()) {
			rta.agregarAlFinal(l2.quitarPrimero());
		}
		return rta;
	}

	public boolean contains(E t){
		boolean rta=false;
		Nodo actual = primero;
		while(!rta &&  actual!=null){
			if(t.equals(actual.darData())){
				rta=true;
			}
			actual=actual.darSiguiente();
		}
		return rta;
	}

	public E remove(E elemento){
		E rta = null;
		if(elemento.equals(primero.darData())){
			rta=primero.darData();
			primero = primero.darSiguiente();
			tam--;
		}
		else{
			Nodo<E> actual = primero;
			boolean encontro = false;
			while(!encontro && actual!=null && actual.darSiguiente() !=null){
				if(actual.darSiguiente().darData().equals(elemento)){
					encontro=true;
					tam--;
				}
				else{
					actual = actual.darSiguiente();
				}
			}
			if(encontro){
				Nodo<E> temp =actual.darSiguiente();
				rta=temp.darData();
				actual.asignarSiguiente(temp.darSiguiente());
			}
		}

		return rta;
	}
	public boolean equals(Lista<E> lista){
		if(lista==null){
			return false;
		}
		boolean rta = tam==lista.tam;
		if(rta){
			Iterator<E> i1 = this.iterator();
			Iterator<E> i2 = lista.iterator();
			while(i1.hasNext() && rta){
				E e1 = i1.next();
				E e2 = i2.next();
				rta = e1.equals(e2);
			}
		}
		return rta;
	}
	protected void agregarOrdenado(E elemento, Comparator<E> c){

		if(primero==null || c.compare(elemento, primero.darData())>=0){
			agregarAlPrincipio(elemento);
		}
		else{
			Nodo<E> n = new Nodo<E>(elemento);
			Nodo<E> actual = primero;
			boolean agrego = false;
			while(!agrego){
				if(actual.darSiguiente()==null){
					actual.siguiente=n;
					agrego=true;
					tam++;
				}
				else if(c.compare(elemento, actual.darSiguiente().darData())>=0){
					n.siguiente=actual.siguiente;
					actual.siguiente=n;
					agrego=true;
					tam++;
				}
				actual=actual.siguiente;
			}
		}


	}
}