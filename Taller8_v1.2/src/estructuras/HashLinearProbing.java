package estructuras;

import java.lang.reflect.Array;

/**
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 *	se borr� el m�todo iterator y se agregaron atributos que muestran m�s 
 *	detalladamente el estado del hash
 */
public class HashLinearProbing<K extends Comparable<K> ,V> 
{
	private static final int INIT_CAPACITY = 4;
	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private K[] keys;      // the keys
	private V[] vals;    // the values

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tama�o del arreglo fijo.
	 */
	private int capacidad;
	/**
	 * Numero de colisiones
	 */
	private int colisiones;

	//Constructores

	public HashLinearProbing(){
		this(INIT_CAPACITY,0.5f );
	}

	@SuppressWarnings("unchecked")
	public HashLinearProbing(int pCapacidad, float pFactorCargaMax) {
		capacidad= pCapacidad;
		factorCargaMax=pFactorCargaMax;
		factorCarga=0;
		vals = (V[]) new Object[capacidad];
		keys = (K[]) new Comparable[capacidad];
		colisiones =0;
	}

	public void put(K llave, V valor){
		// double table size if 50% full
		if (factorCarga > factorCargaMax) resize(2*capacidad);
		int i;
		for (i = hash(llave); keys[i] != null; i = (i + 1) % capacidad, colisiones++) {
			if (keys[i].equals(llave)) {
				vals[i] = valor;
				return;
			}
		}
		keys[i] = llave;
		vals[i] = valor;
		count++;
		factorCarga=(float)count/capacidad;
	}

	public int darCapacidad()
	{
		return capacidad;
	}

	public int size()
	{
		return count;
	}

	public V get(K llave){
		// colisiones
		for (int i = hash(llave); keys[i] != null; i = (i + 1) % capacidad)
			if (keys[i].equals(llave))
				return vals[i];
		return null;
	}

	public V delete(K llave){
		// colisiones
		V retorno= null;
		if ( get(llave) == null) return null;

		// find position i of key
		int i = hash(llave);
		while (!llave.equals(keys[i])) {
			i = (i + 1) % capacidad;
		}

		// delete key and associated value
		keys[i] = null;
		retorno= vals[i];
		vals[i] = null;

		// rehash all keys in same cluster
		i = (i + 1) % capacidad;
		while (keys[i] != null) {
			// delete keys[i] an vals[i] and reinsert
			K   keyToRehash = keys[i];
			V valToRehash = vals[i];
			keys[i] = null;
			vals[i] = null;
			count--;
			put(keyToRehash, valToRehash);
			i = (i + 1) % capacidad;
		}

		count--;
		if (count > 0 && count <= capacidad/8) resize(capacidad/2);
		return retorno;
	}

	//Hash
	private int hash(K llave)
	{
		return (llave.hashCode() & 0x7fffffff) % capacidad;
	}

	private void resize(int pCcapacidad)
	{
		HashLinearProbing<K, V> temp = new HashLinearProbing<K, V>(pCcapacidad, factorCargaMax);
		for (int i = 0; i < capacidad; i++) {

			if (keys[i] != null) {

				temp.put(keys[i], vals[i]);
			}

		}

			keys = temp.keys;
			vals = temp.vals;
			capacidad  = temp.capacidad;
		
	}
	public int darColisiones(){
		return colisiones;
	}
	
	public V[] values(Class<V> c)
	{
		final V[] valores = (V[]) Array.newInstance(c, count);
		int i=0;
		for (V valor: vals) 
		{
			if(valor!=null)
			{
				valores[i]=valor;
				i++;
			}
		}
		return valores;
	}

}