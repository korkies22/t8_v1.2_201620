package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.Estacion;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private HashLinearProbing<K,Nodo<K>> nodos;
	/**
	 * Lista de adyacencia 
	 */
	private HashLinearProbing<K,Lista<Arco<K,E>>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	private int nNodos;
	
	private int nArcos;
	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		nodos= new HashLinearProbing<>();
		adj= new HashLinearProbing<>();
		nNodos=0;
		nArcos=0;
	}

	public int darNumArcos()
	{
		return nArcos;
	}
	
	public int darNumNodos()
	{
		return nNodos;
	}
	
	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		if(nodos.get(nodo.darId())!=null) return false;
		nodos.put(nodo.darId(), nodo);
		adj.put(nodo.darId(), new Lista<Arco<K,E>>());
		nNodos++;
		return true;
	}

	@Override
	public boolean eliminarNodo(K id) {
		if(nodos.get(id)==null) return false;
		nodos.delete(id);
		adj.delete(id);
		nNodos--;
		return true;
	}

	@Override
	public Arco<K,E>[] darArcos() {
		Lista<Arco<K,E>> listaE=new Lista<Arco<K,E>>();
		Lista<Arco<K,E>>[] listArcos= adj.values((Class<Lista<Arco<K, E>>>) listaE.getClass());
		int tam=0;
		for (Lista<Arco<K, E>> arcolist: listArcos) 
		{
			tam+=arcolist.size();
		}
		Arco<K,E>[] arcos= new Arco[tam];
		int i=0;
		for (Lista<Arco<K, E>> arcolist: listArcos) 
		{
			for (Arco arco: arcolist) 
			{
				arcos[i]=arco;
				i++;
			}
		}
		return arcos;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		Nodo<K> e=new Estacion(" ", 0, 0);
		return nodos.values((Class<Nodo<K>>) e.getClass());
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		if(inicio.equals("129A10")) System.out.println("pasa");
		Arco<K,E> arc= crearArco(inicio, fin, costo, obj);
		if(arc==null) return false;
		else
		{
			Lista lista1= adj.get(inicio);
			if(lista1!=null)
			{
				lista1.agregarAlFinal(arc);
				nArcos++;
				return true;
			}
			else
				return false;
		}
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		Lista<Arco<K,E>> act= adj.get(inicio);
		for (Arco<K,E> i: act) 
		{
			if(i.darNodoFin().darId().equals(fin)) act.remove(i);
			nArcos--;
			return i;
		}
		return null;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		return nodos.get(id);
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		Lista<Arco<K,E>> act= adj.get(id);
		int i=0;
		Arco[] arcos= new Arco[act.size()];
		for (Arco arco: act) 
		{
			arcos[i]=arco;
			i++;
		}
		return arcos;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		return null;
	}

}
