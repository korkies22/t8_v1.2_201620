package mundo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido<String, String> grafo;

	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo=new GrafoNoDirigido<String, String>();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		Arco<String, String>[] arcos=grafo.darArcosOrigen(identificador);
		String[] resp= new String[arcos.length];
		for (int i=0; i<arcos.length; i++) 
		{
			resp[i]=arcos[i].darInformacion();
		}
		return resp;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		double min= Double.POSITIVE_INFINITY;
		Arco arcMin= null;
		for (Arco a: grafo.darArcos()) 
		{
			if(a.darCosto()<min)
			{
				min= a.darCosto();
				arcMin=a;
			}
		}
		return min;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		double max= -1;
		Arco arcMax= null;
		for (Arco a: grafo.darArcos()) 
		{
			if(a.darCosto()>max)
			{
				max= a.darCosto();
				arcMax=a;
			}
		}
		return max;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		BufferedReader in= new BufferedReader(new FileReader(RUTA_PARADEROS));
		int nEstaciones= Integer.parseInt(in.readLine());
		for (int i = 0; i < nEstaciones; i++) 
		{
			String[] data= in.readLine().split(";");
			boolean resp=grafo.agregarNodo(new Estacion<String>(data[0],Double.parseDouble(data[1]), Double.parseDouble(data[2])));
			if(!resp) throw new Exception("hubo un problema cargando los nodos");
		}
		System.out.println("Se han cargado correctamente "+grafo.darNumNodos()+" Estaciones");

		//TODO Implementar
		in= new BufferedReader(new FileReader(RUTA_RUTAS));
		int nRutas= Integer.parseInt(in.readLine());
		in.readLine();
		for (int i = 0; i < nRutas; i++) 
		{
			String ruta= in.readLine();
			int cRutas= Integer.parseInt(in.readLine());
			if(cRutas>0)
			{
				String[] inf= in.readLine().split(" ");
				String[] data;
				for (int j = 0; j < cRutas-1; j++) 
				{
					data= in.readLine().split(" ");
					boolean resp=grafo.agregarArco(inf[0], data[0], Double.parseDouble(data[1]), ruta);
					inf[0]=data[0];
					if(!resp) throw new Exception("hubo un problema cargando los arcos");
				}
			}
			in.readLine();
		}
		System.out.println("Se han cargado correctamente "+ grafo.darNumArcos()+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		return (Estacion<String>) grafo.buscarNodo(identificador);
	}

}
